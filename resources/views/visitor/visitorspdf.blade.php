	<div align="center">
		<h1>Visitors Report</h1>
	</div>
	<div align="center">
	<table>
		<tr>
			<th>Id</th>
			<th>MAC</th>
			<th>e-mail</th>
			<th>First Name</th>
			<th>Last Name</th>
		</tr>
		@foreach ($visitors as $visitor)
		<tr>
			<td>{{ $visitor->id }}</td>
			<td>{{ $visitor->mac }}</td>
			<td>{{ $visitor->email }}</td>
			<td>{{ $visitor->first_name }}</td>
			<td>{{ $visitor->last_name }}</td>
		</tr>
		@endforeach
	</table>
	</div>