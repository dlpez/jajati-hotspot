@extends('layout')

@section('content')
	<h1>Visitors List</h1>
	<div class="panel panel-default" style="margin: -15px -15px 5px -15px;">
		<div class="panel-footer">
		{!! Form::model(Request::all(), ['id' => 'fsearch','route' => 'visitor.index', 'method' => 'GET', 'class' => 'navbar-form', 'role' => 'search']) !!}
	        {!! Form::text('mac', null, ['class' => 'form-control', 'placeholder' => 'MAC']) !!}
	        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'e-mail']) !!}
	        {!! Form::text('firstName', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
	        {!! Form::text('lastName', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
      		<button type="submit" class="btn btn-default">Search</button>
	    	<!-- button type="button" class="btn btn-default" id="bepdf">Export PDF</button-->
	    	<button type="button" class="btn btn-default" id="becsv">Export CSV</button>
	    {!! Form::close() !!}
	    </div>
	</div>


	<table class="table table-striped">
		<tr>
			<th></th>
			<th>MAC</th>
			<th>e-mail</th>
			<th>First Name</th>
			<th>Last Name</th>
			<!-- th>Actions</th -->
		</tr>
		@foreach ($visitors as $visitor)
		<tr>
			<td>{{ $visitor->id }}</td>
			<td>{{ $visitor->mac }}</td>
			<td>{{ $visitor->email }}</td>
			<td>{{ $visitor->first_name }}</td>
			<td>{{ $visitor->last_name }}</td>
			<!-- td>
				<a href="" class="btn btn-xs btn-default">
					<span class="glyphicon glyphicon-pencil"></span>
				</a> &nbsp;
				<a href="#!" class="btn btn-xs btn-default btn-delete" aria-id="{{ $visitor->id }}" >
					<span class="glyphicon glyphicon-remove"></span>
				</a> &nbsp;
				<a href="" class="btn btn-xs btn-default" aria-id="{{ $visitor->id }}" >
					<span class="glyphicon glyphicon-file"></span>
				</a>
			</td -->
		</tr>
		@endforeach
	</table>
	<div align="center">
		{!! $visitors->appends(Request::only(['mac', 'email', 'firstName', 'lastName']))->render() !!}
	</div>
@endsection

@section('scripts')
<script>
$(document).ready(function () {
    $('#bepdf').click(function (e) {
	    var myform= $('#fsearch');
	    //alert(myform.attr('action'));
	    var url = myform.attr('action').replace('visitor', 'visitorspdf');
	    myform.attr('action', url);
	    myform.submit();
    });

    $('#becsv').click(function (e) {
	    var myform= $('#fsearch');
	    var prevurl=myform.attr('action');
	    var url = myform.attr('action').replace('visitor', 'visitorscsv	');
	    myform.attr('action', url);
	    myform.submit();
	    myform.attr('action', prevurl);
    });
});
</script>
@endsection