<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Captive Portal - @yield('title')</title>

    <!-- Referencing Bootstrap CSS that is hosted locally -->
    {!! HTML::style('css/bootstrap.min.css') !!}
    {!! HTML::style('css/layout.css') !!}
    {!! HTML::favicon('img/favicon.ico') !!}
  </head>

  <body>
    <header>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">{!! HTML::image('img/logo.png','', array( 'width' => 40, 'height' => 40, 'style'=> 'margin-top:-10px;' )) !!} </a>
          <a class="navbar-brand" href="#">Wifix Hotspot Manager</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
          @if (!Auth::guest())
            <!-- li class="active">
                <a href="{{ url('/sites') }}">Sites <span class="sr-only">(current)</span></a></li-->
            <li><a href="{{ url('/sites') }}">Sites </a></li>
            <li><a href="{{ url('/visitor') }}">Visitors</a></li>
            <li><a href="{{ url('/visit') }}">Visits</a></li>
          @endif
            <!-- li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
                <li class="divider"></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li -->
          </ul>
          <!-- form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Search">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
          </form -->
          <ul class="nav navbar-nav navbar-right">
          @if (Auth::guest())
            <li><a href="{{ url('/auth/login') }}">Login</a></li>
            <!-- li><a href="{{ url('/auth/register') }}">Register</a></li -->
          @else
            <!-- li><a href="{{ url('/admin/register') }}">Sites</a></li -->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
              </ul>
            </li>
          @endif
        </ul>
          <!--ul class="nav navbar-nav navbar-right">
            <li><a href="#">Link</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </li>
          </ul-->
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    </header>
    <div class="container">
      <div class="row">
        @yield('content')
      </div>
    </div>
    <footer class="footer">
      <div class="container">
        <p class="text-muted text-center">{{\Carbon\Carbon::now()->year}}&copy;Jajati, All rights reserved</p>
      </div>
    </footer>

    <!-- script src="https://code.jquery.com/jquery-1.10.2.min.js"></script muyvieja-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    {!! HTML::script('js/bootstrap.min.js') !!}
    @yield('scripts')
  </body>
</html>