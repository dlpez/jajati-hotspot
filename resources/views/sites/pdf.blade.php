<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Example</title>
  </head>
  <body>

    <main>
      <div id="details" class="clearfix">
        <div id="invoice">
          <h1>{{ $site->name }}</h1>
          {!! HTML::image('storage/'.$site->picture, $site->name, array('class' => 'thumb')) !!}
        </div>
      </div>
  </body>
</html>