@extends('layout')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h1> Label: {{ $site->label }} </h1>
					<h4> Name: {{ $site->name }} </h4>
				</div>
				<div class="panel-body">
					<p><b>URL for redirection:</b> <a href="{{ $site->url }}" target="_blank">{{ $site->url }}</a> </p>
					<p><b>Button label:</b> {{ $site->button_label }} </p>
					<p><b>Terms and Conditions:</b> {{ $site->terms }} </p>
					<p><b>Picture:</b>  
						{!! HTML::image('storage/'.$site->picture, $site->name, array('class' => 'thumb')) !!}
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
