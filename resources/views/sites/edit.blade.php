@extends('layout')

@section('content')
<div class="container-fluid">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default">
			<div class="panel-heading">Edit Site</div>
			<div class="panel-body">
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				@if (isset($updated) && $updated == 'yes')
					<div class="alert alert-success">
						<strong>Updated!</strong>
					</div>
				@endif
				{!! Form::model($site, ['route' => ['sites.update', $site], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group">
					    {!! Form::label('name', 'Name') !!}
					    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('label', 'Label') !!}
						{!! Form::text('label',null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('url', 'URL for redirection') !!}
						{!! Form::text('url',null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('button_label', 'Button label') !!}
						{!! Form::text('button_label',null, ['class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('terms', 'Terms and Conditions') !!}
						{!! Form::textarea('terms',null, ['class' => 'form-control']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('picture', 'Picture') !!}
						{!! Form::file('picture', null, ['class' => 'form-control']) !!}
					</div >
					{!! HTML::image('storage/'.$site->picture, $site->name, array('class' => 'thumb')) !!}
					
					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-primary">
								Save
							</button>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
