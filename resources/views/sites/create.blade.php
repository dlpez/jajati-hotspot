@extends('layout')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Register New Site</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('sites') }}" enctype="multipart/form-data">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="" required="required">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Label</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="label" value="" required="required">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">URL for redirection</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="url" value="" required="required">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Button label</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="button_label" value="" required="required">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Terms and Conditions</label>
							<div class="col-md-6">
								<!-- input type="text" class="form-control" name="label" value="" required="required" -->
								<textarea class="form-control" rows="20" name="terms" required="required"></textarea>
							</div>
						</div>

						<div class="form-group">
			              <label class="col-md-4 control-label">Picture</label>
			              <div class="col-md-6">
			                <input type="file" class="form-control" name="picture" accept="image/*" required="required">
			              </div>
			            </div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Register
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
