@extends('layout')

@section('content')
<div class="container-fluid">
		<div class="row">
		<div class="col-md-12 ">
			<div class="panel panel-default">

				<div class="panel panel-default" >
					<div class="panel-footer">
					{!! Form::model(Request::all(), ['route' => 'sites.index', 'method' => 'GET', 'class' => 'navbar-form', 'role' => 'search']) !!}
				        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
				        {!! Form::text('label', null, ['class' => 'form-control', 'placeholder' => 'Label']) !!}
				        <button type="submit" class="btn btn-default">Search</button>
				    {!! Form::close() !!}
				    </div>
				</div>
				
				<div class="panel-heading">Sites list
					<a href="{{ route('sites.create') }}" class="btn btn-md btn-primary" style="float: right; margin-top: -10px;">
						<span class="glyphicon glyphicon-plus"></span>
					</a>
				</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<table class="table table-striped">
						<tr>
							<th></th>
							<th>Name</th>
							<th>Label</th>
							<th>Terms and Conditions</th>
							<th>Url redir.</th>
							<th>Button label</th>
							<th>Actions</th>
						</tr>
						@foreach ($sites as $site)
						<tr>
							<td>{{ $site->id }}</td>
							<td>{{ $site->name }}</td>
							<td>{{ $site->label }}</td>
							<td>{{ $site->terms }}</td>
							<td>{{ $site->url }}</td>
							<td>{{ $site->button_label }}</td>
							<td>
								<a href="{{ route('sites.edit', $site) }}" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span></a> &nbsp;
								<a href="#!" class="btn btn-xs btn-default btn-delete" aria-id="{{ $site->id }}" ><span class="glyphicon glyphicon-remove"></span></a> &nbsp;
								<a href="{{ url('/sites/'.$site->id) }}" class="btn btn-xs btn-default" aria-id="{{ $site->id }}" ><span class="glyphicon glyphicon-file"></span></a>
							</td>
						</tr>
						@endforeach
					</table>
					<div align="center"> 
						{!! $sites->appends(Request::only(['name','label']))->render() !!}
					</div>

					{!! Form::open(array('id' => 'form-delete', 'class' => 'form-inline', 'method' => 'DELETE', 'route' => array('sites.destroy', ':SITE_ID'))) !!}
					{!! Form::close() !!}
					<!-- form method="DELETE" id="form-delete"></form-->
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function () {
    $('.btn-delete').click(function (e) {
	    e.preventDefault();
    	if (confirm("Are you sure?")){

	        var row = $(this).parents('tr');
	        var id = $(this).attr('aria-id');
	        var form = $('#form-delete');
	        var url = form.attr('action').replace(':SITE_ID', id);
	        //var url = 'sites/'+id;
	        var data = form.serialize();


	        $.post(url, data, function (result) {
	            alert(result.message);
	        	row.fadeOut();
	        }).fail(function () {
	            alert('ERROR: not Deleted');
	            row.show();
	        });
    	}
    });
});
</script>
@endsection
