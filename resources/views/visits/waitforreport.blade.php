@extends('layout')
@section('title')
	Visits
@endsection

@section('content')
	<h1>Please Wait</h1>
	<h2>Your report is being built, this may take a few minutes, you will receive an email ({{ Auth::user()->email }}) when it is ready</h2>
@endsection