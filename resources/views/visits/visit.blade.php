@extends('portal')
@section('title')
{{ $visit->site->label }}
@endsection

@section('content')
<div id="hj-portal" class="col-xs-12 col-sm-4 col-sm-offset-4">
    {!! HTML::image('storage/'.$visit->site->picture, $visit->site->name, array('class' => 'thumb')) !!}
    <form name="visitForm" method="POST" novalidate ng-submit="send()" ng-init="visitor.mac = '{{ $visitor->mac }}'; visitor.id = {{ $visitor->id }}; visitId = {{ $visit->id }};">
        <div class="form-group">
            <label for="first_name">
                Nombres
                <span class="error" ng-show="visitForm.first_name.$dirty && visitForm.first_name.$invalid">*</span>
            </label>
            <input type="text" name="first_name" class="form-control" ng-model="visitor.firstName" required/>
        </div>
        <div class="form-group">
            <label for="last_name">
               Apellidos
                <span class="error" ng-show="visitor.lastName.$dirty && visitor.lastName.$invalid">*</span>
            </label>
            <input type="text" name="last_name"  class="form-control" ng-model="visitor.lastName" required/>
        </div>
        <div class="form-group">
            <label for="email">
                Email
                <span class="error" ng-show="visitForm.email.$dirty && visitForm.email.$invalid">*</span>
            </label>
            <input type="email" name="email" class="form-control" ng-model="visitor.email" required />
        </div>
        <div id="hj-terms-of-use" class="form-group">
                <input type="checkbox" ng-model="terms" />
                <label for="accept">Estas aceptando los <a href="#hj-terms-of-use" data-toggle="modal" data-target="#hj-modal-terms-of-use">t&eacute;rminos de uso</a> </label>
            </div>
        <div class="form-group">
            <button type="submit" class="btn btn-info form-control" ng-disabled="!visitor.firstName || !visitor.lastName || !visitor.email">
                {{$visit->site->button_label}}
            </button>
        </div>
        <input type="hidden" name="id" value="{{ $visitor->id }}" ng-model="visitor.id" />
        <input type="hidden" ng-model="visitor.mac" value="{{ $visitor->mac }}">
    </form>
        <div role="alert" class="alert alert-danger" ng-show="! terms">
          <strong>Debe aceptar los terminos y condiciones</strong>
        </div>
    <div role="alert" class="alert alert-success" ng-show="redirecting" >
      <strong>Gracias!!!</strong> Le estamos redirigiendo
    </div>

</div>
<div class="modal fade" id="hj-modal-terms-of-use">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">T&eacute;rminos de uso</h4>
      </div>
      <div class="modal-body">
        {{$visit->site->terms}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Acepto</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop