@extends('layout')
@section('title')
	Visits
@endsection

@section('content')
<div class="container-fluid">
		<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<div class="panel panel-default">
				<div class="panel-heading">Visits list  {{ $visits["query"] }}</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="panel panel-default" style="margin: -15px -15px 5px -15px;">
						<div class="panel-footer" style="margin: 0px;">
						{!! Form::model(Request::all(), ['id' => 'fsearch', 'route' => 'visit.index', 'method' => 'GET', 'class' => 'navbar-form', 'role' => 'search']) !!}
					        {!! Form::text('visitor', null, ['class' => 'form-control', 'placeholder' => 'visitor' ]) !!}
					        {!! Form::select('site', $sites, null, ['class' => 'form-control']) !!}
					        {{-- !! Form::text('ap', null, ['class' => 'form-control', 'placeholder' => 'ap']) !! --}}
					        {!! Form::text('ssid', null, ['class' => 'form-control', 'placeholder' => 'ssid']) !!}
						    {!! Form::text('dateTimeStart',null, ['class' => 'form-control', 'placeholder' => \Carbon\Carbon::create(2000, 1, 1,8,0), 'id' => 'txDateTimeStart']) !!}
					        {!! Form::text('dateTimeEnd',null, ['class' => 'form-control', 'placeholder' => \Carbon\Carbon::tomorrow(), 'id' => 'txDateTimeEnd']) !!}
					        {{-- !! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'url']) !! --}}
					        {!! Form::select('completed',['1' => 'Visit completed', '0' => 'Visit NOT completed'],null,  ['class' => 'form-control']) !!}
				      		<button type="submit" class="btn btn-default" >Search</button>
				      		<button type="button" class="btn btn-default" id="becsv">Export CSV</button>
					    {!! Form::close() !!}
					    </div>
					</div>

					


					<table class="table table-striped">
						<tr>
							<th></th>
							<th><div align="center">First Name </div></th>
							<th><div align="center">Last Name </div></th>
							<th><div align="center">MAC </div></th>
							<th><div align="center">Email </div></th>
							<th><div align="center">Site </div></th>
							<th><div align="center">Ssid </div></th>
							<th><div align="center">Date-time </div></th>
							<th><div align="center">Compl. </div></th>
						</tr>
						@foreach ($visits as $visit)
						<tr>
							<td>{{ $visit->id }}</td>
							<td>{{ $visit->visitor["first_name"] }}</td>
							<td>{{ $visit->visitor["last_name"] }}</td>
							<td>{{ $visit->visitor["mac"] }}</td>
							<td>{{ $visit->visitor["email"] }}</td>
							<td>{{ $visit->site->label. " (".$visit->site->name.")" }}</td>
							<!--td>{{ $visit->ap }}</td -->
							<td>{{ $visit->ssid }}</td>
							<td>{{ $visit->time }}</td>
							<td>{{ $visit->completed==1 ? 'yes' : 'no' }}</td>
						</tr>
						@endforeach

					</table>

					<div align="center"> 
						{!! $visits->appends(Request::only(['visitor', 'site', 'ssid', 'dateTimeStart', 'dateTimeEnd', 'completed']))->render() !!}
					</div>

					{!! Form::open(array('id' => 'form-delete', 'class' => 'form-inline', 'method' => 'DELETE', 'route' => array('visit.destroy', ':visit_ID'))) !!}
					{!! Form::close() !!}
					<!-- form method="DELETE" id="form-delete"></form-->
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
$(document).ready(function () {
    $('.btn-delete').click(function (e) {
	    e.preventDefault();
    	if (confirm("Are you sure?")){

	        var row = $(this).parents('tr');
	        var id = $(this).attr('aria-id');
	        var form = $('#form-delete');
	        var url = form.attr('action').replace(':visit_ID', id);
	        //var url = 'visits/'+id;
	        var data = form.serialize();

	        row.fadeOut();

	        $.post(url, data, function (result) {
	            alert(result.message);
	        }).fail(function () {
	            alert('ERROR: not Deleted');
	            row.show();
	        });
    	}
    });

    $('#becsv').click(function (e) {
	    var myform= $('#fsearch');
	    var prevurl=myform.attr('action');
	    var url = myform.attr('action').replace('visit', 'visitscsv');
	    myform.attr('action', url);
	    myform.submit();
	    myform.attr('action', prevurl);
    });

    $(function() {
    	$( "#txDateTimeStart" ).datepicker({
      		dateFormat: 'yy-mm-dd'
		});
  	});

    $(function() {
    	$( "#txDateTimeEnd" ).datepicker({
      		dateFormat: 'yy-mm-dd'
		});
  	});

});
</script>
@endsection
