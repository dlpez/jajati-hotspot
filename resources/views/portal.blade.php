<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta id="token" name="token" value="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bienvenido - @yield('title')</title>

    <!-- Referencing Bootstrap CSS that is hosted locally -->
    {!! HTML::style('components/bootstrap/dist/css/bootstrap.min.css') !!}
    {!! HTML::style('css/portal.css') !!}
    {!! HTML::favicon('img/favicon.ico') !!}
    {!! HTML::script('components/jquery/dist/jquery.min.js') !!}
    {!! HTML::script('components/bootstrap/dist/js/bootstrap.min.js') !!}
    {!! HTML::script('components/angular/angular.min.js') !!}
    {!! HTML::script('js/controllers.js') !!}
    {!! HTML::script('js/app.js') !!}
    <script>
      var config = angular.module("wifixApp.config", [])
        .constant("CSRF_TOKEN", '{{ csrf_token() }}')
        .constant("URL", '{{ $visit->site->url }}');
    </script>
  </head>

  <body>
    <div class="container">
      <div class="row" ng-app="wifixApp" ng-controller="PortalCtrl">
        @yield('content')
      </div>
    </div>
    <footer class="footer">
          <div class="container">
            <p class="text-muted text-center">{!! HTML::image('img/logo.png','', array( 'width' => 20, 'height' => 20 )) !!} {{\Carbon\Carbon::now()->year}}&copy;Jajati, All rights reserved</p>
          </div>
        </footer>
</body>
</html>