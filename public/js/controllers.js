'use strict';

var wifixControllers = angular.module('wifixControllers', ['wifixApp.config']);
wifixControllers.controller('PortalCtrl', ['$scope', '$http', 'CSRF_TOKEN', 'URL',
    function($scope, $http, CSRF_TOKEN, URL) {
        $scope.terms = true;
        $scope.redirecting = false;

        $scope.send = function  () {
            var data = {
                id : $scope.visitor.id,
                first_name : $scope.visitor.firstName,
                last_name : $scope.visitor.lastName,
                email : $scope.visitor.email,
                mac : $scope.visitor.mac,
                visit_id : $scope.visitId,
                csrf_token : CSRF_TOKEN
            };
            $http.post('/visitor', data).success(function  (data, status) {
                window.location.assign(URL);
            })
        }
}])