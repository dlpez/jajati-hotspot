<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder{

	public function run(){
		\DB::table('users')->insert(array(
			"name" => "jajati",
			"email" => "ja@ja.ti",
			"password" => \Hash::make("jetwebjajati")
		));
	}	

}
