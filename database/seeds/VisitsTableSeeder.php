<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class VisitsTableSeeder extends Seeder{

	public function run(){

		$f= Faker::create();
		for ($i=0; $i < 50000; $i++) { 
			
			\DB::table('hj_visits')->insert(array(
				'ap' => $f->unique()->macAddress,
            	'time' => $f->dateTime($max = 'now'),
            	'url' => $f->url,
            	'ssid' => $f->company,
            	'site_id' => $f->numberBetween(155, 199),
            	'visitor_id' => $f->numberBetween(1, 50),
			));
		}
	}	
}
