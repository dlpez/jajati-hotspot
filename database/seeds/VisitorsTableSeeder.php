<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class VisitorsTableSeeder extends Seeder{

	public function run(){

		$f= Faker::create();
		for ($i=0; $i < 111; $i++) { 
			
			\DB::table('hj_visitors')->insert(array(
				"mac" => $f->unique()->macAddress,
				"email" => $f->unique()->email,
				"first_name" => $f->firstName,
				"last_name" => $f->lastName,
			));
		}
	}	
}
