<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class SitesTableSeeder extends Seeder{

	public function run(){

		$f= Faker::create();
		for ($i=0; $i < 50; $i++) { 
					
			\DB::table('hj_sites')->insert(array(
				"name" => $f->unique()->company,
				"label" => $f->unique()->company,
				"picture" => "img_1.png",
				"terms" => $f->text(1400),
				"url" => $f->url,
				"button_label" => $f->sentence(3),
			));
		}
	}	

}
