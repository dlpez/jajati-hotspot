<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * @author David Lopez <dleo.lopez@gmail.com>
 */
class AddCompletedToVisits extends Migration
{
    /**
     * Add completed column to visits, for state is completed visit
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hj_visits', function (Blueprint $table) {
            $table->boolean('completed')
                ->nullable()
                ->default(false)
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hj_visits', function (Blueprint $table) {
            $table->dropColumn('completed');
        });
    }
}
