<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('hj_visits', function (Blueprint $table) {
            $table->increments('id');
            $table->char('ap', 18);
            $table->dateTime('time');
            $table->string('url');
            $table->string('ssid');
            $table->integer('site_id')->unsigned();
            $table->integer('visitor_id')->unsigned()->nullable();
            $table->timestamps();
        });
        Schema::table('hj_visits', function (Blueprint $table) {
            $table->foreign('site_id')
                ->references('id')->on('hj_sites');
            $table->foreign('visitor_id')
                ->references('id')->on('hj_visitors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hj_visits');
    }
}
