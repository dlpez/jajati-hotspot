<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SitesV2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('hj_sites', function (Blueprint $table) {
            $table->text('terms');
            $table->string('url');
            $table->string('button_label',50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hj_sites', function (Blueprint $table) {
            $table->dropColumn(['terms', 'url', 'button_label']);
        });
   }
}
