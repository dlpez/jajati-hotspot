<?php
/**
 * Created by PhpStorm.
 * User: David probando
 * Date: 6/11/2015
 * Time: 2:11 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Site extends Eloquent {

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hj_sites';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'label', 'url', 'button_label', 'terms'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['picture'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visits()
    {
        return $this->hasMany('App\Visit');
    }

    /**
    */
    public static function filterAndPaginate($name, $label)
    {
        return Site::name($name)
            ->label($label)
            ->orderBy('id', 'ASC')
            ->paginate();
    }

    public function scopeName($query, $name)
    {
        if (trim($name) != "")
        {
            $query->where('name', "LIKE", "%$name%");
        }
    }

    public function scopeLabel($query, $label)
    {
        if (trim($label) != "")
        {
            $query->where('label', "LIKE", "%$label%");
        }
    }

} 

