<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Visit
 * Represent a visit from guest to a site on hotspot
 *
 * @author David Lopez <dlopez@sistelnet.com.ve>
 * @package App
 */
class Visit extends Model
{

    /**
     * @var string
     */
    protected $table = 'hj_visits';

    /* *
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //protected $hidden = ['ap', 'time', 'url', 'ssid', 'site_id', 'visitor_id'];

    protected $visible = ['id', 'ap', 'time', 'url', 'ssid', 'site_id', 'visitor_id'];

    /**
     * This atrributes should be mutated to dates
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'time'];

    protected $cast = [
        'completed' => 'boolean'
    ];

    /**
     * Visit is made for one visitor
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function visitor()
    {
        return $this->belongsTo('App\Visitor');
    }

    /**
     * Site from visitor is acceding
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function site()
    {
        return $this->belongsTo('App\Site');
    }

    public static function filterAndPaginate($ap, $dateTimeStart, $dateTimeEnd, $url, $ssid, $visitor, $site, $completed = true)
    {
        return Visit::applyFilters($ap, $dateTimeStart, $dateTimeEnd, $url, $ssid, $visitor, $site, $completed)
            ->orderBy('hj_visits.id', 'ASC')
            ->paginate();
    }

    public static function filter($ap, $dateTimeStart, $dateTimeEnd, $url, $ssid, $visitor, $site, $completed=true)
    {
        return Visit::applyFilters($ap, $dateTimeStart, $dateTimeEnd, $url, $ssid, $visitor, $site, $completed)
            ->get();
    }

    public static function applyFilters($ap, $dateTimeStart, $dateTimeEnd, $url, $ssid, $visitor, $site, $completed=true)
    {
        return Visit::ap($ap)
            ->url($url)
            ->ssid($ssid)
            ->site($site)
            ->dateTimeStart($dateTimeStart)
            ->dateTimeEnd($dateTimeEnd)
            ->completed($completed)
            ->visitor($visitor);
    }

    public function scopeAp($query, $ap)
    {
        if (trim($ap) != "")
        {
            $query->where('ap', "LIKE", "%$ap%");
        }
    }

    public function scopeUrl($query, $url)
    {
        if (trim($url) != "")
        {
            $query->where('url', "LIKE", "%$url%");
        }
    }

    public function scopeSsid($query, $ssid)
    {
        if (trim($ssid) != "")
        {
            $query->where('ssid', "LIKE", "%$ssid%");
        }
    }

    public function scopeSite($query, $site)
    {
        if (trim($site) != "" && $site != 0 )
        {
            $query->where('site_id', "=", "$site");
        }
    }

    public function scopeDateTimeStart($query, $dateTimeStart)
    {
        if (trim($dateTimeStart) != "")
        {
            $query->where('time', ">=", "$dateTimeStart");
        }
    }

    public function scopeDateTimeEnd($query, $dateTimeEnd)
    {
        if (trim($dateTimeEnd) != "")
        {
            $query->where('time', "<=", "$dateTimeEnd");
        }
    }

    public function scopeVisitor($query, $visitor)
    {
        if (trim($visitor) != "")
        {
            $query->join('hj_visitors', 'hj_visits.visitor_id', '=', 'hj_visitors.id')
            ->whereRaw('(hj_visitors.email LIKE "%'.$visitor.'%" or hj_visitors.first_name LIKE "%'.$visitor.'%" or hj_visitors.last_name LIKE "%'.$visitor.'%"  or hj_visitors.mac LIKE "%'.$visitor.'%")');
        }
    }

    public function scopeCompleted($query, $completed=true)
    {
        $query->where('completed', "=", "$completed");
    }
}
