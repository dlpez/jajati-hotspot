<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Visit;
use App\Http\Requests;
use Mail;

class Report extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $parameters;
    protected $email;

    /**
     * Create a new job instance.
     *
     * @param $parameters
     * @return void
     */
    public function __construct($parameters, $email)
    {
        $this->parameters = $parameters;
        $this->email= $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $r=$this->parameters;
        $visits= Visit::filter(/*ap*/ '', $r['dateTimeStart'], $r['dateTimeEnd'], /*url*/ '', $r['ssid'], $r['visitor'], $r['site'], $r['completed']);
        $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());
        $csv->insertOne(  ['id', "first name", "last name", "Visitor's MAC", "Visitor's email", 'site', 'AP MAC' , 'SSID', 'Date time', 'url', 'completed' ] );
        $visits->each(function($visit) use($csv) {
            $csv->insertOne([$visit->id, $visit->visitor["first_name"], $visit->visitor["last_name"], $visit->visitor["mac"], $visit->visitor["email"], $visit->site->label. " (".$visit->site->name.")", $visit->ap , $visit->ssid, $visit->time, $visit->url, $visit->completed ]);
        });
        $reportname='report'.\Carbon\Carbon::now()->format('_h_i_s').'.csv';
        \Storage::disk('local')->put($reportname, $csv);
        Mail::raw(
                    'Your report is located at '.asset('/storage/'.$reportname).' It will be available for 48 hours and then will be deleted.', 
                    function ($message) {
                        $message->to($this->email, 'Administrator')->subject('Your report is ready');
                    }
        );
    }
}