<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

$router->resource('visitor', 'VisitorController');
$router->resource('visit', 'VisitController');
$router->resource('sites', 'SitesController');

Route::get('sitepdf/{site}', 'SitesController@showpdf');
Route::get('visitorspdf', 'VisitorController@showpdf');
Route::get('visitorscsv', 'VisitorController@downloadcsv');

Route::get('visitscsv', 'VisitController@downloadcsv');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController'
]);
Route::get('guest/s/{site}', 'GuestController@visit');