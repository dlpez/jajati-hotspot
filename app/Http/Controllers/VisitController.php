<?php

namespace App\Http\Controllers;

use App\Visit;
use App\Site;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Jobs\Report;
use Auth;

class VisitController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the visits.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $visits= Visit::filterAndPaginate($request->get('ap'), $request->get('dateTimeStart'), $request->get('dateTimeEnd'), $request->get('url'), $request->get('ssid'), $request->get('visitor'), $request->get('site'), $request->input('completed',1));
        //dd($visits);
        $sites =  array('0' => '---- site ----') + Site::orderBy('label', 'ASC')->lists("label", "id")->toArray();
        return view('visits.index', compact('visits'), compact('sites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the specified resource in pdf format.
     *
     * @param  int  $id
     * @return Response
     */
/*    public function showpdf(Request $request)
    {
        $visitors= Visitor::filter($request->get('mac'), $request->get('email'), $request->get('firstName'), $request->get('lastName'));
        $view =  \View::make("visitor.visitorspdf", compact('visitors'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('visitor.visitorspdf');
    }
*/
    public function downloadcsv(Request $request){
        ini_set("memory_limit", "1024M");
        $visits= Visit::filter($request->get('ap'), $request->get('dateTimeStart'), $request->get('dateTimeEnd'), $request->get('url'), $request->get('ssid'), $request->get('visitor'), $request->get('site'), $request->get('completed'));
        if(count($visits) < 10000){
            $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());
            //$csv->insertOne(\Schema::getColumnListing('hj_visits'));
            $csv->insertOne(  ['id', "first name", "last name", "Visitor's MAC", "Visitor's email", 'site', 'AP MAC' , 'SSID', 'Date time', 'url', 'completed' ] );
            $visits->each(function($visit) use($csv) {
                //$csv->insertOne($visit->toArray() );
                $csv->insertOne([$visit->id, $visit->visitor["first_name"], $visit->visitor["last_name"], $visit->visitor["mac"], $visit->visitor["email"], $visit->site->label. " (".$visit->site->name.")", $visit->ap , $visit->ssid, $visit->time, $visit->url, $visit->completed ]);
            });
            //\Storage::disk('local')->put('report'.\Carbon\Carbon::now().'.csv',  $csv);
            return $csv->output('visits.csv');
        }
        else{
            $this->dispatch(new Report( $request->all(), Auth::user()->email ));
            return view("visits.waitforreport");
        }
    }
}