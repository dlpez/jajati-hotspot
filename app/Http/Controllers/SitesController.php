<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Site;

class SitesController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $sites= Site::filterAndPaginate($request->get('name'), $request->get('label'));
        return view("sites.index", compact('sites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view("sites.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $file = $request->file('picture');
        $filename = $request->get('name')."_1.".$file->getClientOriginalExtension();//$file->getClientOriginalName();

        $site= new Site($request->all());
        $site->picture=$filename;
        $site->save();
        //
        \Storage::disk('local')->put($filename,  \File::get($file));
        return redirect()->route('sites.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $site= Site::findOrFail($id);
        //$site->picture= 'http://hots.pot/storage/'.$site->picture;
        return view("sites.view", compact('site'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $site= Site::findOrFail($id);
        //dd($site);
        return view('sites.edit')->with('site', $site);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */

    public function update(Request $request, $id)
    {
        $site= Site::findOrFail($id);
        $site->fill($request->all());
        if($request->file('picture') != ""){
            $file = $request->file('picture');
            $filename = $request->get('name')."_1.".$file->getClientOriginalExtension();
            \Storage::disk('local')->put($filename,  \File::get($file));
            $site->picture=$filename;
        }
        $site->save();

        //Session::flash('updated', 'yes');
        //return redirect()->back();
        return view('sites.edit')->with(['site' => $site, 'updated' => 'yes']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $site = Site::find($id);
        //dd($site);
        $site->delete();
        if ($request->ajax()) {
            return response()->json([
                'id'      => $id,
                'message' => 'deleted '.$site->name
            ]);
        }

        return redirect()->route('/');
    }

    /**
     * Display the specified resource in pdf format.
     *
     * @param  int  $id
     * @return Response
     */
    public function showpdf($id)
    {
        $site= Site::findOrFail($id);
        //$site->picture= 'http://hots.pot/storage/'.$site->picture;
        $view =  \View::make("sites.pdf", compact('site'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('sites.pdf');

        //return view("sites.view", compact('site'));
    }
}
