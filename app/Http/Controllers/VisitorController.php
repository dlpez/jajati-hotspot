<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateVisitorRequest;
use App\Library\UnifiApi;
use App\Site;
use App\Visit;
use App\Visitor;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VisitorController extends Controller
{


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth', ['except' => ['postStore']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $visitors= Visitor::filterAndPaginate($request->get('mac'), $request->get('email'), $request->get('firstName'), $request->get('lastName'));
        return view('visitor.index', compact('visitors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * This store a visitor
     *
     * @param Requests\CreateVisitorRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Requests\CreateVisitorRequest $request)
    {
        $success = true;
        $visitor = Visitor::findOrFail($request->input('id'));
        $visitor->first_name = $request->input('first_name');
        $visitor->last_name  = $request->input('last_name');
        $visitor->email      = $request->input('email');
        $visitor->mac        = $request->input('mac');
        $visitor->save();

        $visit            = Visit::findOrFail($request->input('visit_id'));
        $visit->completed = true;
        $visit->save();


        $api = new UnifiApi();
        $api->site = $visit->site->name;
        if ($api->login()){
            if (!$api->authorizeGuest($visitor->mac, env('UNIFI_GUEST_MINUTES', 30))) {
                //throw new \Exception('No se pudo autorizar el invitado');
                $success = false;
            }
        } else {
            //throw new \Exception('No se pudo comunicar con el controlador');
            $success = false;
        }
        # TODO: Remove this pause, just temporal
        sleep(5);
        return compact('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /* *
     * This will record url sent from unifi controller and handle as visit.
     * If site doesn't exist it will create. The same with Visitor.
     *
     * @param $site
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
   /* public function visit($site, Request $request)
    {
        $url  = $request->input('url');
        $site = Site::where('name', $site)->first();
        $visitor = Visitor::firstOrCreate(['mac' => $request->input('id')]);
        if (empty($site)) {
            return redirect($url);
        }
        $visit             = new Visit();
        $visit->ap         = $request->input('ap');
        $visit->url        = $url;
        $time              = new \DateTime();
        $visit->time       = $time->setTimestamp($request->input('t'));
        $visit->ssid       = $request->input('ssid');
        $visit->site_id    = $site->id;
        $visit->visitor_id = $visitor->id;
        $visit->save();

        return view('visits.visit', compact('visit', 'visitor'));
    }
*/
    /**
     * Display the specified resource in pdf format.
     *
     * @param  int  $id
     * @return Response
     */
    public function showpdf(Request $request)
    {
        $visitors= Visitor::filter($request->get('mac'), $request->get('email'), $request->get('firstName'), $request->get('lastName'));
        //$visitors= Visitor::filterAndPaginate('A', '', '', '');
        //dd($visitors);
        $view =  \View::make("visitor.visitorspdf", compact('visitors'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('visitor.visitorspdf');
    }

    public function downloadcsv(Request $request, Response $r){
        $visitors= Visitor::filter($request->get('mac'), $request->get('email'), $request->get('firstName'), $request->get('lastName'));
        $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());
        $csv->insertOne(\Schema::getColumnListing('hj_visitors'));
        $visitors->each(function($visitor) use($csv) {
            $csv->insertOne($visitor->toArray());
        });
        return $csv->output('visitors.csv');
    }

}
