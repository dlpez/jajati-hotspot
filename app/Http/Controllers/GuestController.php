<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Library\UnifiApi;
use App\Site;
use App\Visit;
use App\Visitor;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * This will record url sent from unifi controller and handle as visit.
     * If site doesn't exist it will create. The same with Visitor.
     *
     * @param $site
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function visit($site, Request $request)
    {
        $url  = $request->input('url');
        $site = Site::where('name', $site)->first();
        $visitor = Visitor::firstOrNew(['mac' => $request->input('id')]);
        $visitor->mac = $request->input('id');
        $visitor->save();
        if (empty($site)) {
            return redirect($url);
        }
        $visit             = new Visit();
        $visit->ap         = $request->input('ap');
        $visit->url        = $url;
        $time              = new \DateTime();
        $visit->time       = $time->setTimestamp($request->input('t'));
        $visit->ssid       = $request->input('ssid');
        $visit->site_id    = $site->id;
        $visit->visitor_id = $visitor->id;
        $visit->completed  = false;
        $visit->save();

        return view('visits.visit', compact('visit', 'visitor'));
    }
}
