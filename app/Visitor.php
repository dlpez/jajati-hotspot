<?php
/**
 * Created by PhpStorm.
 * User: David
 * Date: 6/11/2015
 * Time: 2:05 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Visitor
 * @package App
 */
class Visitor extends Eloquent {

    /**
     * @var string
     */
    protected $table = 'hj_visitors';

    /**
     * @var array
     */
    protected $fillable = ['email','first_name','last_name'];

    /* *
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //protected $hidden = ['mac'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visits()
    {
        return $this->hasMany('App\Visit');
    }

    public static function filterAndPaginate($mac, $email, $firstName, $lastName)
    {
        return Visitor::applyFilters($mac, $email, $firstName, $lastName)
            ->paginate();
    }

    public static function filter($mac, $email, $firstName, $lastName)
    {
        return Visitor::applyFilters($mac, $email, $firstName, $lastName)
            ->get();
    }

    public static function applyFilters($mac, $email, $firstName, $lastName)
    {
        return Visitor::mac($mac)
            ->email($email)
            ->firstName($firstName)
            ->lastName($lastName)
            ->orderBy('id', 'ASC');
    }

    public function scopeMac($query, $mac)
    {
        if (trim($mac) != "")
        {
            $query->where('mac', "LIKE", "%$mac%");
        }
    }

    public function scopeEmail($query, $email)
    {
        if (trim($email) != "")
        {
            $query->where('email', "LIKE", "%$email%");
        }
    }

    public function scopeFirstName($query, $firstName)
    {
        if (trim($firstName) != "")
        {
            $query->where('first_name', "LIKE", "%$firstName%");
        }
    }

    public function scopeLastName($query, $lastName)
    {
        if (trim($lastName) != "")
        {
            $query->where('last_name', "LIKE", "%$lastName%");
        }
    }
}