<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;

class DeleteFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DeleteFiles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes csv files from public/storage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //deful location at filesystem.php 'root' => public_path().'/storage'
        $file_names = Storage::files();
        foreach ($file_names as $file_name) {
            if( ends_with($file_name, '.csv') ){
                $file_date= \Carbon\Carbon::createFromTimeStamp( Storage::lastModified($file_name) );
                //delete report files older than 48 hours
                if($file_date->diffInHours(\Carbon\Carbon::now()) > 48)
                    Storage::delete($file_name);
            }        
        }
    }
}
